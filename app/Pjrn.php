<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pjrn extends Model
{
    protected $table = 'pelajaran';

     protected $fillable = 	[
								'id',
								'nis',
								'nama',
								'rombel',
								'rayon',
								'jk',
								'mtk',
								'indo',
								'inggris',
								'prod'
     						];

}
