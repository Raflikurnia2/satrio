<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pelajaran', 'Pelajaran@index');
Route::get('/pelajaran/create', 'Pelajaran@create');
Route::post('/pelajaran/store', 'Pelajaran@store');
Route::get('/edit/{id}', 'Pelajaran@edit');
Route::post('/pelajaran/update/{id}', 'Pelajaran@update');

// eror
Route::get('/delete/{id}', 'Pelajaran@delete');
