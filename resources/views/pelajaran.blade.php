@extends('layouts.lte')

@section('content')
<!-- <div style="padding-left: 1002px">

</div> -->
<div class="box-body">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
              <br>
              	<a href="/pelajaran/create" style="MARGIN-left: 1002px" type="button" class="btn btn-primary btn-default btn-sm"><i class="fa fa-fw fa-cart-plus"></i> Create</a>
            </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                	<th>No</th>
                  	<th>NIS</th>
                  	<th>Nama</th>
                  	<th>Rombel</th>
                  	<th>Rayon</th>
                  	<th>JK</th>
                  	<th>Matematika</th>
                  	<th>B.Indonesia</th>
                  	<th>B.Inggris</th>
                  	<th>Produktif</th>
                  	<th>Action</th>
                </tr>
                </thead>
                <tbody>
                	@foreach ($pelajaran as $key => $p)
                	{{ csrf_field() }}
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$p->nis}}</td>
                  <td>{{$p->nama}}</td>
                  <td>{{$p->rombel}}</td>
                  <td>{{$p->rayon}}</td>
                  <td>{{$p->jk}}</td>
                  <td>{{$p->mtk}}</td>
                  <td>{{$p->indo}}</td>
                  <td>{{$p->inggris}}</td>
                  <td>{{$p->prod}}</td>
                  <td>
                  	<a type="button" href="{{URL('/edit/'.$p->id)}}" class="btn btn-warning btn-default btn-sm"><i class="fa fa-edit"> Edit</i></a>
                  	<a onclick="return confirm('Do You Want To Delete Data {{$p->nama}}??')" href="{{URL('/delete/'.$p->id)}}" type="button" class="btn btn-danger btn-default btn-sm"><i class="fa fa-fw fa-trash"></i>Delete</a>
                  </td>
                </tr>
            </tbody>
            @endforeach
           </table>
            </div>
        </div>
@endsection
