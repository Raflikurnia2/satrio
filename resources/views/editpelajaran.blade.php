@extends('layouts.lte')

@section('content')
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ URL('/pelajaran/update/'.$p->id)}}" method="post">
            	{{ csrf_field() }}
              <div class="box-body">
                <input name="id" type="hidden" value="{{ $p->id }}">
                <div class="form-group">
                  <label for="exampleInputEmail1">NIS</label>
                  <input type="number" class="form-control" name="nis" id="exampleInputEmail1" value="{{ $p->nis}}" placeholder="Enter NIS">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama</label>
                  <input type="text" class="form-control" name="nama" id="exampleInputPassword1" value="{{ $p->nama}}" placeholder="Enter Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Rombel</label>
                  <input type="text" class="form-control" name="rombel" id="exampleInputPassword1" value="{{ $p->rombel}}" placeholder="Enter Rombel">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Rayon</label>
                  <input type="text" class="form-control" name="rayon" id="exampleInputPassword1" value="{{ $p->rayon}}" placeholder="Enter Rayon">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Jenis Kelamin</label>
                  <select class="form-control" value="{{ $p->jk}}" name="jk">
                  	<OPTION>Laki-Laki</OPTION>
                  	<OPTION>Perempuan</OPTION>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">MTK</label>
                  <input type="number" name="mtk" value="{{ $p->mtk}}" class="form-control" id="exampleInputEmail1" placeholder="Nilai MTK">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Indonesia</label>
                  <input type="number" name="indo" value="{{ $p->indo}}" class="form-control" id="exampleInputEmail1" placeholder="Nilai Indonesia">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Inggris</label>
                  <input type="number" name="inggris" value="{{ $p->inggris}}" class="form-control" id="exampleInputEmail1" placeholder="Nilai Inggris">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Produktif</label>
                  <input type="number" name="prod" class="form-control" value="{{ $p->prod}}" id="exampleInputEmail1" placeholder="Nilai Produktif">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
            <a href="/pelajaran" onclick="return confirm('Are You Sure?')" class="btn btn-danger">Cancel</a>
              </div>
          </div>
@endsection